# Short Intro to WASM
## Web Assembly in C

My first programming language was 8080 assembly. I was hand 
compiling 8080 asm long before I learned BASIC. I have a soft 
spot for assembly language programming. Though it isn't worth
the effort in most software development these days. Not like
it was back in the 80's and 90's. Back then we often mixed 
assembly with BASIC, C or C++ to make our code run faster.
Today, compilers have come a long way and now most can 
optimize code far better than an expert assembly developer.

The web however, isn't quite as mature and speed is often an
issue. As web application become larger and more complex, the
browser struggle to keep up. Requests get delayed and process
time slow to a crawl. So developers started looking for a 
faster alternative. Enter WebAssemby or WASM. WASM is a 
standardized assembly language that runs in the browser on
the client side. It's smaller than Javascript and faster too!
However, like all assembly languages WASM isn't for the 
faint of heart. It's not easy to grasp if you're not used
to programming at lowest level. If you don't understand
machine architecture then WASM may be a bit more difficult
for you to grasp. 

### High level languages to the Rescue!
WASM may be a bit difficult and isn't meant for your every
day developer. If you're not interested in learning to write
WASM directly, then try one of the many high level 
languages that can be transpiled to WASM. In this demo
I'll use C and Emscripten to compile a simple C program
to WASM and execute it in the browser. This is a simple
prrof of concept and should get you started.

### Setting Up your Environment
I'm running Ubuntu 18.04 and was surprised to find that
the repo's current version of Emscripten dates back to 
2014. This version is too old to include WASM support.
So the first step is to install the latest version of
Enscripten. 

_If you're running Windows or iOS check out:_

* _https://emscripten.org/docs/getting_started/downloads.html#installation-instructions_

_For more information on setting up Emscripten on_ 
_your system._


To do this on Linux first make sure you have the latest
version of the following packages installed:

* python-2.7
* nodejs
* cmake
* default-jre

After you have confirmed these are installed on your 
system, run the following commands in the terminal:
(Note "$>" is used to represent the command promt.)

$> cd ~

$> mkdir emsdk

$> cd emsdk

$> git clone https://github.com/emscripten-core/emsdk.git

$> cd emsdk

$> git pull

$> ./emsdk install latest

$> ./emsdk activate latest

$> source ./emsdk_env.sh

With that completed you should now be ready to write
your C program. We are goingto start off easy and do
the traditional hello world app in C. 

Create a directory to contain your project. I called 
mine hello-wasm:

$> mkdir hello-wasm

$> cd hello-wasm

Then cd into your new directory. Next we will create
a new C file entitled hello.c

$> touch hello.c

Now we need to add our program code to our new file:

$> nano hello.c

Once the file is open in the editor, add the 
following C code:

#include <stdio.h>

int main(int argc, char ** argv) {
   printf("Hello WASM!\n");
}


Now save the file with cntrl+O, and exit nano
with  cntrl+X. 

Now we have a simple C program we need to compile
it into WASM. TO do this we run the following 
command:

$> emcc hello.c -s WASM=1 -o hello.html

This will compile our C program into WASM and
create some supporting files. If you list the files
in the folder now you'll find:

* hello.wasm
* hello.html
* hello.js
* perhaps others...

When we ran our compile command we passed a few
flags. The -s WASM=1 flag tells emcc to compile
to WASM format. The -o hello.html flag tells
emcc to call the main output file hello.html.
The other supporting files created by emcc should
all begin with "hello" or what ever name you
gave the program.

### Running The Sample
Now we have a compiled WASM program that we can 
run in the browser. All we need is a capable
web browser. Just load the hello.html file in
to your browser. You should see a terminal window
with the text "Hello WASM!" displayed in it.

If not, make sure your code is correct. Note that
I had to include a carriage return "\n" at the
end of the text to make it work. I think this is 
a bug in my version of emcc. But if you get errors
be sure to include them in your code as I did above.

Now let's have a little more fun! Re-open hello.c
in nano and edit it to read as follows:

#include <stdio.h>

int main(int argc, char ** argv) {
   printf("Hello WASM!\n");

   for(int i=0; i < 10; i++) {
      printf("%d", i);
   }
}

Now save the code and recompile it using the same 
command as before:

$> emcc heelo.c -s WASM=1 -o hello.html

Now refresh the page in your browser. You should
now see the Hello WASM! message as well as a list
of numbers from 0 to 9.

Congradualtions, you've compled a C program for 
the WEB. 

